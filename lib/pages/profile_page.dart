import 'package:face_recognition/pages/home_page.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({Key key, @required this.username}) : super(key: key);
  final username;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Welcome back, $username !"),
      ),
      body: Container(
        child: Column(
          children: [
            Text("This is your super amazing profile"),
            RaisedButton(
              child: Text("Logout"),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
