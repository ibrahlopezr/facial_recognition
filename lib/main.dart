import 'package:flutter/material.dart';
import 'package:face_recognition/pages/home_page.dart';
import 'package:firebase_core/firebase_core.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FacialApp();
  }
}

class FacialApp extends StatelessWidget {
  const FacialApp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        //visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class Cargando extends StatelessWidget {
  const Cargando({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        //visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: Text("Cargando..."),
        ),
      ),
    );
  }
}

class ShowError extends StatelessWidget {
  final dynamic error;
  const ShowError({Key key, this.error}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        //visualDensity: VisualDensity.adaptivePlatformDensity,
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Center(
          child: Column(
            children: [
              Center(
                child: Text(
                  "Ocurrio un error!!",
                  style: TextStyle(color: Colors.red),
                ),
              ),
              Center(
                child: Text(
                  "$error",
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
